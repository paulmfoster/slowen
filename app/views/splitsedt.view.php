<?php include VIEWDIR . 'head.view.php'; ?>

<!-- This screen is for single transactions with possible splits. -->

<form action="<?php echo $return; ?>" method="post">
<?php $form->hidden('txntype'); ?>
<?php $form->hidden('txnid'); ?>

<?php $txn = $txns[0]; ?>

<h3>Transaction ID: <?php echo $txn['txnid']; ?></h3>

<table>

<tr>
<td class="tdlabel">From Acct</td>
<td><?php echo $txn['from_acct'] . ' ' . $txn['from_acct_name']; ?></td>
</tr>

<tr>
<td class="tdlabel">Date</td>
<td><?php $form->date('txn_dt'); ?></td>
<td>

<tr>
<td class="tdlabel">Check No</td>
<td><?php $form->text('checkno'); ?></td>
</tr>

<tr>
<td class="tdlabel">Payee</td>
<td><?php $form->select('payee_id'); ?></td>
</tr>

<tr>
<td class="tdlabel">Memo</td>
<td><?php $form->text('memo'); ?></td>
</tr>

<tr>
<td class="tdlabel">Category/Acct</td>
<td><?php $form->select('to_acct'); ?></td>
</tr>

<tr>
<td class="tdlabel">Status</td>
<td><?php echo $txn['x_status']; ?></td>
</tr>

<tr>
<td class="tdlabel">Recon Dt</td>
<?php $recondt = new xdate(); ?>
<td><?php echo $recondt->iso2amer($txn['recon_dt']); ?></td>
</tr>

<tr>
<td class="tdlabel">Amount</td>
<td>
<?php echo int2dec($txn['amount']); ?>
</td>
</tr>

<?php for ($k = 0; $k < $max_splits; $k++): ?>
<?php $form->hidden('split_id', $splits[$k]['id']); ?>

<tr>
<td class="tdlabel">Split Payee <?php echo $k + 1; ?></td>
<td><?php $form->select('split_payee_id', $splits[$k]['payee_id']); ?></td>
</tr>

<tr>
<td class="tdlabel">Split To Acct <?php echo $k + 1; ?></td>
<td><?php $form->select('split_to_acct', $splits[$k]['to_acct']); ?></td>
</tr>

<tr>
<td class="tdlabel">Split Memo <?php echo $k + 1; ?></td>
<td><?php $form->text('split_memo', $splits[$k]['memo']); ?></td>
</tr>

<tr>
<td class="tdlabel">Split Amount <?php echo $k + 1; ?></td>
<td><?php $form->text('split_amount', int2dec($splits[$k]['amount'])); ?></td>
</tr>

<?php endfor; ?>

</table>

<p>
<?php $form->submit('save'); ?>
&nbsp;
<?php form::abandon('showtxn.php?txnid=' . $txn['txnid']); ?>
</p>


</form>

<?php include VIEWDIR . 'footer.view.php'; ?>

